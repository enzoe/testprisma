import { Prisma } from "prisma-binding";

const prisma = new Prisma({
  typeDefs: "src/generated/prisma.graphql",
  endpoint: "http://localhost:4466"
});

const createPostAndFetchUser = async(authorId, data) => {
  const userExists = await prisma.exists.User({
    id: authorId
  });

  if (!userExists) {
    throw new Error('Unable to find user');
  };
  
  const post = await prisma.mutation.createPost({
    data: {
      ...data,
      author: {
        connect: {
          id: authorId
        }
      }
    }
  }, '{ author {id name email posts {id title published}}}');
  
  return post.author;
}

/* createPostAndFetchUser("ck3xziko501iy07210sbrr4cf", {
  title: "This is a title",
  body: "Wow",
  published: true
})
  .then(user => console.log(JSON.stringify(user, undefined, 2)))
  .catch(error => console.log(error.message)) */

const updatePostForUser = async(postId, data) => {

  const postExists = await prisma.exists.Post({
    id: postId
  });

  if (!postExists) {
    throw new Error("Unable to find post");
  };

  const post = await prisma.mutation.updatePost({
    where: {
      id: postId
    },
    data
  }, '{author {id name email posts {id title published}}}')

  return post.author;
}

/* updatePostForUser("ck3zwq9dx08ap0721to4i87befff", {title: "this text was updated dasfas"})
  .then(user => console.log(JSON.stringify(user, undefined, 2)))
  .catch(error => console.log(error.message)) */


/* prisma.query.users(null, "{id name posts {id title}}")
  .then(data => console.log(JSON.stringify(data, undefined, 2))); */

/* prisma.query.comments(null, "{id text author {id name}}")
  .then(data => console.log(JSON.stringify(data, undefined, 2))); */


/* prisma.mutation.updatePost({
  where: {
    id: "ck3z331y7006s0721ezghm9u4"
  },
  data: {
    body: "changed post to published",
    published: true
  }
}, "{id published}").then(data => {
  return prisma.query.posts(null, "{id title body published}");
}).then(data => {
  console.log(data);
}) */